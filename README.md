# DogRecognitionServer

Servidor en Flask que recibe imágenes y reconoce si hay perros

## Preparar el entorno
**Windows o Linux**
> python -m venv DogImgRecognitionServer

## Activar el entorno
**Windows**
> DogImgRecognitionServer\Scripts\activate
**Linux**
> source DogImgRecognitionServer/bin/activate

## Desactivar el entorno
> deactivate

## Instalar las dependencias
Para que el servidor funcione, se debe instalar los paquetes listados en el archivo _requirements.txt_.

Para hacer esto debes ejecutar el siguiente comando.
> pip install -r requirements.txt
